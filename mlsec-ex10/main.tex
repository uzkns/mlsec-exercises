\documentclass[a4paper, DIV=16, 11pt, twocolumn]{scrartcl}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[numbers,sort&compress]{natbib}
\usepackage{microtype}
\usepackage{libertinus}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{svg}
\usepackage{csquotes}
\usepackage{amsfonts}
\usepackage{tabularx}
\usepackage{tablefootnote}
\usepackage{todonotes}
\usepackage{array,ragged2e}
\usepackage{eso-pic}
\usepackage{booktabs}

\usepackage[style=numeric]{biblatex}
\addbibresource{thesis.bib}


\hypersetup{
    colorlinks=false,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    pdftitle={MLsec: Exercise 10},
    pdfpagemode=FullScreen,
    pdfstartview=Fit
}

\title{Machine Learning for Computer Security: Exercise 10}
\subtitle{Summary of paper \frqq{}"Why Should I Trust You? Explaining the Predictions of Any Classifier"\flqq{}\cite{tulio}}
\author{Maximilian F. Göckel $<$uzkns@student.kit.edu$>$}



% Define a command to set the background image
\newcommand\BackgroundPic{
  \put(0,0){
    \parbox[b][\paperheight]{\paperwidth}{
      \vfill
      \centering
      \includegraphics[width=\paperwidth, height=\paperheight, keepaspectratio]{images/bg.png}
      \vfill
    }
  }
}

\begin{document}

% Add the background image to all pages
%\AddToShipoutPicture*{\BackgroundPic}



\twocolumn[
  \begin{@twocolumnfalse}
    \maketitle
    %\abstract{Type abstract here.}
\ \\
\ \\

    %\tableofcontents
  \end{@twocolumnfalse}
]
%\clearpage
%\pagebreak



The paper "Why Should I Trust You? Explaining the Predictions of Any Classifier" by Ribeiro et al., introduces LIME (Local Interpretable Model-agnostic Explanations), a novel framework designed to explain the predictions of any classifier in an interpretable and faithful manner. The core idea of LIME is to approximate any black-box classifier with an interpretable model (such as a linear model or a decision tree) locally around the prediction. By doing so, LIME provides insights into why the model made a specific prediction for an individual instance, rather than offering a global understanding of the model.\\

\section{Problem Setting}
The main problem revolves around the challenge of making the predictions of machine learning (ML) models understandable to humans, especially when those models are complex and operate as "black boxes." Despite the increasing power and prevalence of ML models in various domains, including healthcare, finance, and even criminal justice\cite{justice}, their lack of transparency and interpretability often leads to mistrust and reluctance in adoption among end-users and decision-makers. The core issue stems from the difficulty in comprehending how these models arrive at their predictions, which is crucial for validating their accuracy, fairness, and reliability.\\

\section{Solution}
The authors work on explaining the predictions of any classifier, regardless of its complexity and without requiring access to its internal workings. The aim is to generate explanations that are both interpretable to humans and faithful to how the model makes its decisions. The focus lies on individual predictions rather than the model's overall behavior.\\

The authors introduce a new framework called LIME (short for \textit{Local Interpretable Model-agnostic Explanations}). LIME is designed to explain the predictions of any classifier in a way that is understandable to humans, regardless of the complexity or type of the underlying model. The key innovation of LIME is its ability to create model-agnostic explanations, meaning it can be applied to any machine learning model without needing to know its internal workings.

The approach taken by LIME involves approximating the complex, often non-linear decision boundary of any classifier with a simple, interpretable model around the vicinity of the prediction being explained. This is achieved by perturbating the input data sample and observing how the predictions change with these perturbations. By selectively weighting these perturbed samples based on their proximity to the original sample, LIME focuses on the local decision boundary around the instance being explained. It then trains an interpretable model, such as a linear regression or a decision tree, on this locally weighted dataset to approximate how the original model makes predictions for similar instances.

This interpretable model, which is simple enough for humans to understand, serves as the explanation for the prediction. It highlights which features of the input were most influential in the model's decision-making process, providing insights into why the model arrived at its specific prediction for that instance. LIME's explanations can be tailored to various forms of data, including tabular data, text, and images, making it versatile across many applications.\\

\section{Problems with the Proposed Solution}
The proposed solution also presents several problems and limitations.

Firstly, the reliability of LIME's explanations depends heavily on the quality and relevance of the perturbed samples generated around the instance being explained. If these samples do not accurately represent the decision boundary of the original model in that local region, the explanations generated may be misleading or incorrect. This is particularly challenging in high-dimensional spaces or for complex models where the decision boundary cannot be easily approximated with simple models.

Secondly, LIME is inherently local in its explanations, focusing on individual predictions. While this is useful for understanding specific decisions made by a model, it does not provide insight into the model's overall behavior or its performance across a broader range of instances. Users might incorrectly generalize the explanations provided for one instance to the model as a whole, leading to misconceptions about how the model operates in different contexts.

Another issue is the selection of the interpretable model and its complexity. The choice of a simple linear model or decision tree to approximate the decision boundary of the original model might not always capture the intricacies of that boundary, even locally. This is espcially true for complex models, like deep neural networks.

\section{Conclusion}
Local Interpretable Model-agnostic Explanations (LIME) offers a valuable approach to demystifying the predictions of complex machine learning models, fostering greater transparency and trust in artificial intelligence systems. By generating interpretable explanations for individual predictions, LIME bridges the gap between the advanced capabilities of modern AI models and the need for human-understandable reasoning. Despite facing challenges such as the potential for misleading explanations, limitations in capturing global model behavior, and computational demands, LIME represents a crucial step towards making AI more accessible and accountable.

%\clearpage
%\pagebreak

\addcontentsline{toc}{chapter}{\bibname}

\printbibliography

\end{document}

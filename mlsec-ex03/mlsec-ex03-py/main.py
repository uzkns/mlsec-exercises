import matplotlib.pyplot as plt
import numpy as np

#sanitized beforehand by removing punctuation
str_a = "They call it a Royale with cheese"
str_b = "A Royale with cheese What do they call a Big Mac"
str_c = "Well a Big Mac is a Big Mac but they call it le Big-Mac"
str_d = "Le Big-Mac Ha ha ha ha What do they call a Whopper"

strings = [str_a.split(" "), str_b.split(" "), str_c.split(" "), str_d.split(" ")]
matrices = []

def main():
    for d in range(1, 5):
        matrix = [[], [], [], []]
        for s1 in range(0, len(strings)):
            for s2 in range(0, len(strings)):
                matrix[s1].append(kernel(strings[s1], strings[s2], d))
        matrices.append(matrix.copy())

    for m in matrices:
        for bla in m:
            print(bla)
        print("---------")
        __prettyprint(m)
    return 0



def kernel(x, z, d):
    lang = __combine(x, z)
    ctr = 0
    for l in lang:
        x_ctr = 0
        z_ctr = 0
        for w in x:
            if w == l:
                x_ctr += 1
        for w in z:
            if w == l:
                z_ctr += 1
        ctr = ctr + (x_ctr * z_ctr)
    return pow(ctr, d)

def __combine(x, z):
    ret = z.copy()
    for x_word in x:
        if x_word in z:
            pass
        else:
            ret.append(x_word)
    return ret

def __prettyprint(matrix):
    m = np.array(matrix)
    labels = ["They call it...", "A Royale with...", "Well, a Big Mac...", "Le Big-Mac..."]

    fig, ax = plt.subplots()
    im = ax.imshow(m, cmap="copper")

    # Show all ticks and label them with the respective list entries
    ax.set_xticks(np.arange(len(labels)), labels=labels)
    ax.set_yticks(np.arange(len(labels)), labels=labels)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(labels)):
        for j in range(len(labels)):
            text = ax.text(j, i, m[i, j],
                           ha="center", va="center", color="w")

    ax.set_title("Heat map")
    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()


\documentclass[a4paper, DIV=16, 11pt, twocolumn]{scrartcl}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[numbers,sort&compress]{natbib}
\usepackage{microtype}
\usepackage{libertinus}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{svg}
\usepackage{csquotes}
\usepackage{amsfonts}
\usepackage{tabularx}
\usepackage{tablefootnote}
\usepackage{todonotes}
\usepackage{array,ragged2e}
\usepackage{eso-pic}
\usepackage{booktabs}

\usepackage[style=numeric]{biblatex}
\addbibresource{thesis.bib}


\hypersetup{
    colorlinks=false,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    pdftitle={MLsec: Exercise 09},
    pdfpagemode=FullScreen,
    pdfstartview=Fit
}

\title{Machine Learning for Computer Security: Exercise 09}
\subtitle{Summary of paper \frqq{}NEUZZ: Efficient Fuzzing with Neural Program Smoothing\flqq{}\cite{she2019neuzz}}
\author{Maximilian F. Göckel $<$uzkns@student.kit.edu$>$}



% Define a command to set the background image
\newcommand\BackgroundPic{
  \put(0,0){
    \parbox[b][\paperheight]{\paperwidth}{
      \vfill
      \centering
      \includegraphics[width=\paperwidth, height=\paperheight, keepaspectratio]{images/bg.png}
      \vfill
    }
  }
}

\begin{document}

% Add the background image to all pages
%\AddToShipoutPicture*{\BackgroundPic}



\twocolumn[
  \begin{@twocolumnfalse}
    \maketitle
    %\abstract{Type abstract here.}
\ \\
\ \\

    %\tableofcontents
  \end{@twocolumnfalse}
]
%\clearpage
%\pagebreak



The paper "NEUZZ: Efficient Fuzzing with Neural Program Smoothing" addresses the inefficiency of traditional fuzzing techniques in detecting bugs and vulnerabilities in software. It introduces a novel approach using neural networks (NNs) for program smoothing, which transforms the non-linear, discontinuous behavior of real-world programs into smoother functions that are easier to navigate. This method enables more efficient gradient-guided optimization in fuzzing, leading to improved bug and vulnerability detection. The results show a significant improvement in fuzzing efficiency compared to conventional methods.\\

\section{Problem Setting}
In the context of fuzzing a program, a non-smooth, discontinuous function refers to the unpredictable and irregular behavior of a program when various inputs are tested. In real-world software, the program's response to different inputs often contains abrupt changes and discontinuities, making it difficult for traditional fuzzing methods to effectively navigate and optimize the exploration of the program's behavior. This irregularity hinders the ability of fuzzers to systematically and efficiently find bugs and vulnerabilities because they cannot rely on smooth gradients to guide their search through the input space (The input space in fuzzing refers to the range of all possible inputs that can be fed into a program to test its behavior.)\\

\section{Solution}
The authors' approach focuses on using NNs to create a smoothed representation of a program's behavior. This technique transforms the complex, nonlinear, and discontinuous behavior of the program into a more predictable form. This allows the fuzzer to use gradient-based optimization methods to systematically and efficiently explore the input space.\\

This is achieved by training the NNs with test inputs to approximate the behavior of the target program. This training enables the NNs to predict the program's responses to different inputs, smoothing out abrupt changes and discontinuities. This smoothed representation of the program's behavior makes it possible to apply gradient-guided optimization techniques, which are more efficient at navigating the input space and identifying vulnerabilities than traditional fuzzing methods.\\

\section{Problems with the Proposed Solution}
Using NNs to transform non-smooth functions into smooth representations in fuzzing comes with several challenges: First, the complexity of neural networks can lead to overfitting, where the network models the training data too closely and does not generalize well to unseen data. This can cause the fuzzer to miss certain types of defects that were not adequately represented in the training data. The effectiveness of this approach also depends heavily on the accuracy of the neural network model. If the model does not accurately approximate the behavior of the program, the fuzzer could be led astray, spending time exploring irrelevant parts of the input space or missing critical vulnerabilities. In addition, this technique uses incremental learning, where the network is continuously updated with new data. The challenge is to ensure that the network doesn't forget previously learned information (a problem known as catastrophic forgetting) while incorporating new inputs.

\section{Results}
She et al. report that their method outperforms traditional fuzzing techniques in several ways. The authors provide quantitative results showing that NEUZZ is more efficient at finding bugs and vulnerabilities. They measure performance in terms of the number of bugs found and the time it takes to find them, and compare it to other popular fuzzing tools. NEUZZ demonstrates a significant improvement in both speed and effectiveness of bug detection, highlighting the benefits of using neural network-based program smoothing in the fuzzing process. Table \ref{tab:results} shows a comparison between NEUZZ and other state-of-the-art fuzzers.\\
\begin{table}[h!]
\centering
\begin{tabular}{lcccc}
\hline
            & base64 & md5sum & uniq & who  \\ \hline
\#Bugs      & 44     & 57     & 28   & 2,136 \\
FUZZER      & 7      & 2      & 7    & 0     \\
SES         & 9      & 0      & 0    & 18    \\
VUzzer      & 17     & 1      & 27   & 50    \\
Steelix     & 43     & 28     & 24   & 194   \\
Angora      & 48     & 57     & 29   & 1,541 \\
AFL-laf-intel & 42   & 49     & 24   & 17    \\
T-fuzz      & 43     & 49     & 26   & 63    \\
NEUZZ       & 48     & 60     & 29   & 1,582 \\ \hline
\end{tabular}
\caption{Comparison of number of bugs found by different fuzzers}
\label{tab:results}
\end{table}

\section{Conclusion}
The paper "NEUZZ: Efficient Fuzzing with Neural Program Smoothing" shows the effectiveness of using NNs for program smoothing in the fuzzing process. This approach improves the efficiency of fuzzing by smoothing out non-linear and discontinuous program behaviors, allowing for better navigation of the input space and more effective bug detection. The results demonstrate that this method outperforms traditional fuzzing techniques.

%\clearpage
%\pagebreak

\addcontentsline{toc}{chapter}{\bibname}

\printbibliography

\end{document}

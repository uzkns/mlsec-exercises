import pycparser
import hashlib

class ASTNodePrinter(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.indent_level = 0

    def show_node(self, node):
        """ Show node type and contents """
        indent = '  ' * self.indent_level
        node_content = ' '.join(f'{attr}={getattr(node, attr)}' for attr in node.attr_names)
        print(f'{indent}{node.__class__.__name__}: {node_content}')

    def generic_visit(self, node):
        self.show_node(node)

        if node.children():
            self.indent_level += 1
            for _, child in node.children():
                self.generic_visit(child)
            self.indent_level -= 1
def hash_node(node):
    """ Hash a node using its string representation """
    node_str = str(node)
    return hashlib.sha256(node_str.encode()).hexdigest()

def traverse_and_hash(node):
    """ Traverse the AST and compute the hash """
    if node is None:
        return ''
    hash_current = hash_node(node)
    for child in node:
        hash_current += traverse_and_hash(child)
    return hash_current

# C code snippet
c_code = """
void foo()
{
    int x = source();
    if (x < MAX)
    {
        int y = 2 * x;
        sink(y);
    }
}
"""

# Parse the C code to create an AST
parser = pycparser.c_parser.CParser()
ast = parser.parse(c_code)

# Hash the AST
ast_hash = traverse_and_hash(ast)

print("AST Hash:", ast_hash)

# Print the AST
printer = ASTNodePrinter()
printer.visit(ast)

from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import glob


DEGREE = [1,2,3,4]

def bag_of_words(text):
    # Convert text to bag-of-words representation (word frequencies)
    words = text.split()
    word_freq = Counter(words)
    return word_freq


def polynomial_bow_kernel(text1, text2, degree, constant=1):
    # Get bag-of-words representations for both texts
    bow1 = bag_of_words(text1)
    bow2 = bag_of_words(text2)

    # Calculate the dot product between the two bag-of-words vectors
    dot_product = sum(bow1[word] * bow2[word] for word in bow1 if word in bow2)

    # Apply the polynomial kernel formula
    kernel_value = (dot_product + constant) ** degree
    return kernel_value


def compute_center_of_mass(texts, degree, constant=1):
    # Initialize an empty matrix to store kernel values
    kernel_matrix = np.zeros((len(texts), len(texts)))

    # Compute the kernel matrix
    for i in range(len(texts)):
        for j in range(len(texts)):
            kernel_matrix[i, j] = polynomial_bow_kernel(texts[i], texts[j], degree, constant)

    # Calculate the center of mass (mean) along the rows (axis=0)
    center_of_mass = np.mean(kernel_matrix, axis=0)
    return center_of_mass


def load_texts(mode: str):
    texts = []
    for path in glob.glob("/home/mgoeckel/Documents/KIT/MLSec/ex05/task3/ex05-data/spam-train/*." + mode + ".txt"):
        f = open(path, "r", encoding="latin-1")
        texts.append(f.read())
        f.close()
    return texts



def calculate_euclidean_distance(object_text, texts, degree, kxi_xj, constant=1):
    # Calculate the number of texts
    n = len(texts)

    # Calculate the kernel values
    kxx = polynomial_bow_kernel(object_text, object_text, degree, constant)
    kx_xi = np.array([polynomial_bow_kernel(object_text, text, degree, constant) for text in texts])

    # Calculate the Euclidean distance using the kernel trick formula
    return kxx - (2 / n) * np.sum(kx_xi) + (1 / (n**2)) * np.sum(kxi_xj)

def classify(input: list, thresh: int):
    returnlist = []
    for l in input:
        if l > thresh:
            returnlist.append(True)
        else:
            returnlist.append(False)
    return returnlist

if __name__ == '__main__':
    # calcuate center of mass for spam and real mails
    spam_texts = load_texts("spam")
    real_texts = load_texts("ham")

    for degr in DEGREE:
        results = []
        real_classes = []
        kxi_xj_real = np.array([[polynomial_bow_kernel(text1, text2, degr, 1) for text2 in real_texts] for text1 in real_texts])
        kxi_xj_spam = np.array([[polynomial_bow_kernel(text1, text2, degr, 1) for text2 in spam_texts] for text1 in spam_texts])
        for path in glob.glob("/home/mgoeckel/Documents/KIT/MLSec/ex05/task3/ex05-data/spam-test/*.txt"):

            if path.find("spam.txt") > -1:
                real_classes.append(True)
            else:
                real_classes.append(False)

            f = open(path, "r", encoding="latin-1")
            filecontent = f.read()
            results.append(calculate_euclidean_distance(filecontent, real_texts, degr, kxi_xj_real) - calculate_euclidean_distance(filecontent, spam_texts, degr, kxi_xj_spam))
            f.close()


        value_list = []
        for thresh in results:
            performance = zip(real_classes, classify(results, thresh))
            tpr = 0
            fpr = 0
            for z in list(performance):
                if z == (True, True):
                    tpr = tpr + 1
                elif z == (False, True):
                    fpr = fpr + 1
            value_list.append((tpr/len(results), fpr/len(results)))


        xs = [x for x, y in value_list]
        ys = [y for x, y in value_list]
        plt.plot(ys, xs, legend="d=%d" % (degr))
        plt.title('function f_3')

        plt.ylabel('TPR')
        plt.xlabel('FPR')
        plt.show()